package fr.emse.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MoneyTest {
	
	static Money m12CHF;
	static Money m14CHF;
	
	@BeforeAll
	public static void initialisation () {
		m12CHF= new Money(12, "CHF");
		m14CHF= new Money(14, "CHF");	
	}
	
	@Test
	public void testSimpleAdd() {
		Money expected = new Money(26, "CHF");
		Money result = m12CHF.add(m14CHF); 
		//System.out.println(expected.amount());
		//System.out.println(result.amount());
		assertTrue(expected.equals(result));
	}
	
	@Test
	public void testEquals() {
		assertTrue(!m12CHF.equals(null));
		assertEquals(m12CHF, m12CHF);
		assertEquals(m12CHF, new Money(12, "CHF"));
		assertTrue(!m12CHF.equals(m14CHF));
	}
}
